package com.capgemini.algorithmbattles.ballsapp.logic.model;


import java.io.*;
import java.math.BigDecimal;
import java.math.MathContext;

import static java.lang.Math.pow;

public class Network implements Serializable {

    public final int [] NETWORK_LAYER_SIZES;
    public final int INPUT_SIZE;
    public final int OUTPUT_SIZE;
    public final int NETWORK_SIZE;

    private double [][] output;
    double [][][] weights;
    double [][] bias;

    private double [][] error_signals;
    private double [][] output_derivatives;



    public Network(int... NETWORK_LAYER_SIZES){
        this.NETWORK_LAYER_SIZES = NETWORK_LAYER_SIZES;
        this.INPUT_SIZE = NETWORK_LAYER_SIZES[0];
        this.NETWORK_SIZE = NETWORK_LAYER_SIZES.length;
        this.OUTPUT_SIZE = NETWORK_LAYER_SIZES[NETWORK_SIZE-1];

        this.output = new double [NETWORK_SIZE][];
        this.weights = new double[NETWORK_SIZE][][];
        this.bias = new double[NETWORK_SIZE][];

        this.error_signals = new double[NETWORK_SIZE][];
        this.output_derivatives = new double[NETWORK_SIZE][];

        for(int i = 0; i<NETWORK_SIZE; i++){
            this.output[i] = new double[NETWORK_LAYER_SIZES[i]];
            this.error_signals[i] = new double[NETWORK_LAYER_SIZES[i]];
            this.output_derivatives[i] = new double[NETWORK_LAYER_SIZES[i]];

            this.bias[i] = NetworkTools.createRandomArray(NETWORK_LAYER_SIZES[i], 0, 1);
            if(i > 0){
                this.weights[i] = NetworkTools.createRandomArray(NETWORK_LAYER_SIZES[i], NETWORK_LAYER_SIZES[i-1], -1, 1);
            }
        }
    }

    public double[] calculate(double... input){
        if(input.length != this.INPUT_SIZE) return null;
        this.output[0] = input;
        for(int layer = 1; layer < NETWORK_SIZE; layer++){
            for(int neuron = 0; neuron < NETWORK_LAYER_SIZES[layer]; neuron++){
                double sum = bias[layer][neuron];
                for(int prevNeuron = 0; prevNeuron < NETWORK_LAYER_SIZES[layer-1]; prevNeuron++){
                    sum+= output[layer-1][prevNeuron] * weights[layer][neuron][prevNeuron];
                }
                output[layer][neuron] = sigmoid(sum);
                output_derivatives[layer][neuron]= output[layer][neuron]*(1-output[layer][neuron]);
            }
        }
        return output[NETWORK_SIZE-1];
    }
    private double sigmoid(double o){
        return 1d/(1+Math.exp(-o));
    }

    public void learn(double [] input, double [] target, double eta){
        if(input.length != INPUT_SIZE || target.length != OUTPUT_SIZE) return;
        calculate(input);
        backpropagationError(target);
        updateWeights(eta);
    }

    public void backpropagationError(double[] target){
        for(int neuron = 0; neuron < NETWORK_LAYER_SIZES[NETWORK_SIZE-1]; neuron++){
            error_signals[NETWORK_SIZE-1][neuron] = (output[NETWORK_SIZE-1][neuron]- target[neuron])*output_derivatives[NETWORK_SIZE-1][neuron];
        }
        for(int layer = NETWORK_SIZE-2; layer > 0; layer--){
            for(int neuron = 0; neuron < NETWORK_LAYER_SIZES[layer]; neuron++){
                double sum = 0;
                for(int nextNeuron = 0; nextNeuron < NETWORK_LAYER_SIZES[layer+1]; nextNeuron++){
                    sum += weights[layer+1][nextNeuron][neuron] * error_signals[layer+1][nextNeuron];
                }
                this.error_signals[layer][neuron] = sum * output_derivatives[layer][neuron];
            }
        }
    }

    public void updateWeights(double eta){
        for(int layer = 1; layer < NETWORK_SIZE; layer++){
            for(int neuron = 0; neuron < NETWORK_LAYER_SIZES[layer]; neuron++){
                double delta = -eta * error_signals[layer][neuron];
                bias[layer][neuron] += delta;
                for(int prevNeuron = 0; prevNeuron < NETWORK_LAYER_SIZES[layer-1]; prevNeuron++){
                    weights[layer][neuron][prevNeuron] += delta * output[layer-1][prevNeuron];
                }
            }
        }
    }

    public void save(String filename){
        try(ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream(filename))){
            out.writeObject(this);
            out.flush();
        }catch(IOException e){
            e.printStackTrace();
        }
    }
    public void load(String filename){
        try(ObjectInputStream in = new ObjectInputStream(new FileInputStream(filename))){
            Network net = (Network)in.readObject();
            this.output = net.output;
            this.bias = net.bias;
            this.weights = net.weights;
            this.error_signals = net.error_signals;
            this.output_derivatives = net.output_derivatives;
            System.gc();
        }catch(IOException | ClassNotFoundException e){
            e.printStackTrace();
        }
    }

}
