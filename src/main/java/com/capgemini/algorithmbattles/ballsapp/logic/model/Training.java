package com.capgemini.algorithmbattles.ballsapp.logic.model;

import java.util.*;

public class Training {

    public static double[][] Algorithm2(int[][] tab) {
        //tabs for storing dots, first is for vertical, other is for horizontal
        LinkedList<BoardCell>[] algorithm_ver = new LinkedList[tab.length];
        LinkedList<BoardCell>[] opponent_ver = new LinkedList[tab.length];
        LinkedList<BoardCell>[] algorithm_hor = new LinkedList[tab.length];
        LinkedList<BoardCell>[] opponent_hor = new LinkedList[tab.length];
        //tabs for diagonal dots
        LinkedList<BoardCell>[] algorithm_diag_ver = new LinkedList[(tab.length / 2) + 1];
        LinkedList<BoardCell>[] opponent_diag_ver = new LinkedList[(tab.length / 2) + 1];
        LinkedList<BoardCell>[] algorithm_diag_hor = new LinkedList[(tab.length / 2) + 1];
        LinkedList<BoardCell>[] opponent_diag_hor = new LinkedList[(tab.length / 2) + 1];
        for (int i = 0; i < algorithm_ver.length; i++) {
            algorithm_ver[i] = new LinkedList<>();
            opponent_ver[i] = new LinkedList<>();
            opponent_hor[i] = new LinkedList<>();
            algorithm_hor[i] = new LinkedList<>();
        }
        for (int i = 0; i < algorithm_diag_hor.length; i++) {
            algorithm_diag_ver[i] = new LinkedList<>();
            algorithm_diag_hor[i] = new LinkedList<>();
            opponent_diag_ver[i] = new LinkedList<>();
            opponent_diag_hor[i] = new LinkedList<>();
        }
        final int OPPONENT = -1;
        final int ALGORITHM = 1;

        double[][] result = new double[tab.length][tab[0].length];
        for (int i = 0; i < tab.length; i++) {
            for (int j = 0; j < tab[0].length; j++) {
                result[i][j] = tab[i][j];
            }
        }
        //horizontal & vertical dots (i = rows, j = columns)
        for (int i = 0; i < tab.length; i++) {
            for (int j = 0; j < tab[0].length; j++) {
                BoardCell cell = new BoardCell(i, j, null);
                if (tab[i][j] == ALGORITHM) {
                    algorithm_hor[i].addFirst(cell);
                    algorithm_ver[j].addFirst(cell);
                } else if (tab[i][j] == OPPONENT) {
                    opponent_hor[i].addFirst(cell);
                    opponent_ver[j].addFirst(cell);
                }
            }
        }
        //diagonal dots (Integer.MAX_VALUE for corners)
        int[][] tab_diag = new int[(tab.length / 2 + 1)][(tab.length / 2) + 1];
        tab_diag[0][0] = Integer.MAX_VALUE;
        tab_diag[tab_diag.length - 1][0] = Integer.MAX_VALUE;
        tab_diag[0][tab_diag.length - 1] = Integer.MAX_VALUE;
        tab_diag[tab_diag.length - 1][tab_diag.length - 1] = Integer.MAX_VALUE;
        //lower diagonal half
        int sum = 4;
        while (sum != 10) {
            for (int i = 0; i < (tab.length / 2) + 1; i++) {
                for (int j = 0; j < (tab[0].length / 2) + 1; j++) {
                    if (i + j == sum) {
                        BoardCell cell = new BoardCell(i, j, null);
                        if (tab[i][j] == ALGORITHM) {
                            tab_diag[i][j] = ALGORITHM;
                            algorithm_diag_hor[i].addFirst(cell);
                            algorithm_diag_ver[j].addFirst(cell);
                        } else if (tab[i][j] == OPPONENT) {
                            tab_diag[i][j] = OPPONENT;
                            opponent_diag_hor[i].addFirst(cell);
                            opponent_diag_ver[j].addFirst(cell);
                        }
                    }
                }
            }
            sum++;
        }
        //upper diagonal half
        while (sum != 19) {
            for (int i = (tab.length / 2) + 1; i < tab.length; i++) {
                for (int j = (tab[0].length / 2) + 1; j < tab[0].length; j++) {
                    BoardCell cell = new BoardCell(i, j, null);
                    if (i + j == sum) {
                        if (tab[i][j] == ALGORITHM) {
                            tab_diag[i - tab_diag.length][j - tab_diag[0].length] = ALGORITHM;
                            algorithm_diag_hor[i - tab_diag.length].addFirst(cell);
                            algorithm_diag_ver[j - tab_diag[0].length].addFirst(cell);
                        } else if (tab[i][j] == OPPONENT) {
                            tab_diag[i - tab_diag.length][j - tab_diag[0].length] = OPPONENT;
                            opponent_diag_hor[i - tab_diag.length].addFirst(cell);
                            opponent_diag_ver[j - tab_diag[0].length].addFirst(cell);
                        }
                    }
                }
            }
            sum++;
        }
        for (int i = 0; i < tab.length; i++) {
            if (i <= tab.length / 2) {
                algorithm_diag_hor[i].sort(new Comparator<BoardCell>() {
                    @Override
                    public int compare(BoardCell o1, BoardCell o2) {
                        return o1.getY() - o2.getY();
                    }
                });
            }

            algorithm_hor[i].sort(new Comparator<BoardCell>() {
                @Override
                public int compare(BoardCell o1, BoardCell o2) {
                    return o1.getY() - o2.getY();
                }
            });
            if (i <= tab.length / 2) {
                opponent_diag_hor[i].sort(new Comparator<BoardCell>() {
                    @Override
                    public int compare(BoardCell o1, BoardCell o2) {
                        return o1.getY() - o2.getY();
                    }
                });
            }

            opponent_hor[i].sort(new Comparator<BoardCell>() {
                @Override
                public int compare(BoardCell o1, BoardCell o2) {
                    return o1.getY() - o2.getY();
                }
            });
            if (i <= tab.length / 2) {
                algorithm_diag_ver[i].sort(new Comparator<BoardCell>() {
                    @Override
                    public int compare(BoardCell o1, BoardCell o2) {
                        return o1.getX() - o2.getX();
                    }
                });
            }

            algorithm_ver[i].sort(new Comparator<BoardCell>() {
                @Override
                public int compare(BoardCell o1, BoardCell o2) {
                    return o1.getX() - o2.getX();
                }
            });
            if (i <= tab.length / 2) {
                opponent_diag_ver[i].sort(new Comparator<BoardCell>() {
                    @Override
                    public int compare(BoardCell o1, BoardCell o2) {
                        return o1.getX() - o2.getX();
                    }
                });
            }

            opponent_ver[i].sort(new Comparator<BoardCell>() {
                @Override
                public int compare(BoardCell o1, BoardCell o2) {
                    return o1.getX() - o2.getX();
                }
            });
        }

        //looking for victory
        for (int i = 0; i < algorithm_hor.length; i++) {
            if (fourInARow(algorithm_hor[i]) != Integer.MAX_VALUE) {
                //return new BoardCell(i, fourInAColumn(algorithm_hor[i]), null);
                result[i][fourInARow(algorithm_hor[i])] = ALGORITHM;
                return result;
            } else if (fourInAColumn(algorithm_ver[i]) != Integer.MAX_VALUE) {
                //return new BoardCell(fourInAColumn(algorithm_ver[i]), i, null);
                result[fourInAColumn(algorithm_ver[i])][i] = ALGORITHM;
                return result;
            } else if (i <= tab.length / 2 && fourInAColumn(algorithm_diag_ver[i]) != Integer.MAX_VALUE) {
                //return new BoardCell(fourInAColumn(algorithm_diag_ver[i]), i, null);
                result[fourInAColumn(algorithm_diag_ver[i])][i] = ALGORITHM;
                return result;
            } else if (i <= tab.length / 2 && fourInARow(algorithm_diag_hor[i]) != Integer.MAX_VALUE) {
                //return new BoardCell(i, fourInAColumn(algorithm_diag_hor[i]), null);
                result[i][fourInARow(algorithm_diag_hor[i])] = ALGORITHM;
                return result;
            }
        }

        //looking for danger
        for (int i = 0; i < algorithm_hor.length; i++) {
            if (fourInARow(opponent_hor[i]) != Integer.MAX_VALUE) {
                //return new BoardCell(i, fourInAColumn(opponent_hor[i]), null);
                result[i][fourInARow(opponent_hor[i])] = ALGORITHM;
                return result;
            } else if (fourInAColumn(opponent_ver[i]) != Integer.MAX_VALUE) {
                //return new BoardCell(fourInAColumn(opponent_ver[i]), i, null);
                result[fourInAColumn(opponent_ver[i])][i] = ALGORITHM;
                return result;
            } else if (i <= tab.length / 2 && fourInAColumn(opponent_diag_ver[i]) != Integer.MAX_VALUE) {
                //return new BoardCell(fourInAColumn(opponent_diag_ver[i]), i, null);
                result[fourInAColumn(opponent_diag_ver[i])][i] = ALGORITHM;
                return result;
            } else if (i <= tab.length / 2 && fourInARow(opponent_diag_hor[i]) != Integer.MAX_VALUE) {
                //return new BoardCell(i, fourInAColumn(opponent_diag_hor[i]), null);
                result[i][fourInARow(opponent_diag_hor[i])] = ALGORITHM;
                return result;
            }
        }

        //other move
        int max = 0;
        LinkedList<BoardCell> where_to_move = null;
        String origin = "";

        for (LinkedList<BoardCell> list : algorithm_hor) {
            if (list.size() > max) {
                max = list.size();
                where_to_move = list;
                origin = "ah";
            }
        }
        for (LinkedList<BoardCell> list : algorithm_ver) {
            if (list.size() > max) {
                max = list.size();
                where_to_move = list;
                origin = "av";
            }
        }
        for (LinkedList<BoardCell> list : algorithm_diag_hor) {
            if (list.size() > max) {
                max = list.size();
                where_to_move = list;
                origin = "adh";
            }
        }
        for (LinkedList<BoardCell> list : algorithm_diag_ver) {
            if (list.size() > max) {
                max = list.size();
                where_to_move = list;
                origin = "adv";
            }
        }

        if (max == 0) {
            Random rand = new Random();
            //return new BoardCell(rand.nextInt(10), rand.nextInt(10), null);
            result[rand.nextInt(10)][rand.nextInt(10)] = ALGORITHM;
            return result;
        } else {
            BoardCell resultCell = findNext(where_to_move, origin);
            if (resultCell == null || resultCell.getY() > 10 || resultCell.getX() > 10) {
                Random rand = new Random();
                int x = rand.nextInt(10);
                int y = rand.nextInt(10);
                while (tab[x][y] != 0) {
                    x = rand.nextInt(10);
                    y = rand.nextInt(10);
                }
                resultCell = new BoardCell(x, y, null);
            }
            //return resultCell;
            result[resultCell.getX()][resultCell.getY()] = ALGORITHM;
            return result;
        }

    }

    public static BoardCell Algorithm2Cell(int[][] tab) {
        //tabs for storing dots, first is for vertical, other is for horizontal
        LinkedList<BoardCell>[] algorithm_ver = new LinkedList[tab.length];
        LinkedList<BoardCell>[] opponent_ver = new LinkedList[tab.length];
        LinkedList<BoardCell>[] algorithm_hor = new LinkedList[tab.length];
        LinkedList<BoardCell>[] opponent_hor = new LinkedList[tab.length];
        //tabs for diagonal dots
        LinkedList<BoardCell>[] algorithm_diag_ver = new LinkedList[(tab.length / 2) + 1];
        LinkedList<BoardCell>[] opponent_diag_ver = new LinkedList[(tab.length / 2) + 1];
        LinkedList<BoardCell>[] algorithm_diag_hor = new LinkedList[(tab.length / 2) + 1];
        LinkedList<BoardCell>[] opponent_diag_hor = new LinkedList[(tab.length / 2) + 1];
        for (int i = 0; i < algorithm_ver.length; i++) {
            algorithm_ver[i] = new LinkedList<>();
            opponent_ver[i] = new LinkedList<>();
            opponent_hor[i] = new LinkedList<>();
            algorithm_hor[i] = new LinkedList<>();
        }
        for (int i = 0; i < algorithm_diag_hor.length; i++) {
            algorithm_diag_ver[i] = new LinkedList<>();
            algorithm_diag_hor[i] = new LinkedList<>();
            opponent_diag_ver[i] = new LinkedList<>();
            opponent_diag_hor[i] = new LinkedList<>();
        }
        final int OPPONENT = -1;
        final int ALGORITHM = 1;

        double[][] result = new double[tab.length][tab[0].length];
        for (int i = 0; i < tab.length; i++) {
            for (int j = 0; j < tab[0].length; j++) {
                result[i][j] = tab[i][j];
            }
        }
        //horizontal & vertical dots (i = rows, j = columns)
        for (int i = 0; i < tab.length; i++) {
            for (int j = 0; j < tab[0].length; j++) {
                BoardCell cell = new BoardCell(i, j, null);
                if (tab[i][j] == ALGORITHM) {
                    algorithm_hor[i].addFirst(cell);
                    algorithm_ver[j].addFirst(cell);
                } else if (tab[i][j] == OPPONENT) {
                    opponent_hor[i].addFirst(cell);
                    opponent_ver[j].addFirst(cell);
                }
            }
        }
        //diagonal dots (Integer.MAX_VALUE for corners)
        int[][] tab_diag = new int[(tab.length / 2 + 1)][(tab.length / 2) + 1];
        tab_diag[0][0] = Integer.MAX_VALUE;
        tab_diag[tab_diag.length - 1][0] = Integer.MAX_VALUE;
        tab_diag[0][tab_diag.length - 1] = Integer.MAX_VALUE;
        tab_diag[tab_diag.length - 1][tab_diag.length - 1] = Integer.MAX_VALUE;

        //lower diagonal half
        int sum = 4;
        while (sum != 10) {
            for (int i = 0; i < (tab.length / 2) + 1; i++) {
                for (int j = 0; j < (tab[0].length / 2) + 1; j++) {
                    if (i + j == sum) {
                        BoardCell cell = new BoardCell(i, j, null);
                        if (tab[i][j] == ALGORITHM) {
                            tab_diag[i][j] = ALGORITHM;
                            algorithm_diag_hor[i].addFirst(cell);
                            algorithm_diag_ver[j].addFirst(cell);
                        } else if (tab[i][j] == OPPONENT) {
                            tab_diag[i][j] = OPPONENT;
                            opponent_diag_hor[i].addFirst(cell);
                            opponent_diag_ver[j].addFirst(cell);
                        }
                    }
                }
            }
            sum++;
        }
        //upper diagonal half
        while (sum != 19) {
            for (int i = (tab.length / 2) + 1; i < tab.length; i++) {
                for (int j = (tab[0].length / 2) + 1; j < tab[0].length; j++) {
                    BoardCell cell = new BoardCell(i, j, null);
                    if (i + j == sum) {
                        if (tab[i][j] == ALGORITHM) {
                            tab_diag[i - tab_diag.length][j - tab_diag[0].length] = ALGORITHM;
                            algorithm_diag_hor[i - tab_diag.length].addFirst(cell);
                            algorithm_diag_ver[j - tab_diag[0].length].addFirst(cell);
                        } else if (tab[i][j] == OPPONENT) {
                            tab_diag[i - tab_diag.length][j - tab_diag[0].length] = OPPONENT;
                            opponent_diag_hor[i - tab_diag.length].addFirst(cell);
                            opponent_diag_ver[j - tab_diag[0].length].addFirst(cell);
                        }
                    }
                }
            }
            sum++;
        }

        for (int i = 0; i < tab.length; i++) {
            if (i <= tab.length / 2) {
                algorithm_diag_hor[i].sort(new Comparator<BoardCell>() {
                    @Override
                    public int compare(BoardCell o1, BoardCell o2) {
                        return o1.getY() - o2.getY();
                    }
                });
            }

            algorithm_hor[i].sort(new Comparator<BoardCell>() {
                @Override
                public int compare(BoardCell o1, BoardCell o2) {
                    return o1.getY() - o2.getY();
                }
            });
            if (i <= tab.length / 2) {
                opponent_diag_hor[i].sort(new Comparator<BoardCell>() {
                    @Override
                    public int compare(BoardCell o1, BoardCell o2) {
                        return o1.getY() - o2.getY();
                    }
                });
            }

            opponent_hor[i].sort(new Comparator<BoardCell>() {
                @Override
                public int compare(BoardCell o1, BoardCell o2) {
                    return o1.getY() - o2.getY();
                }
            });
            if (i <= tab.length / 2) {
                algorithm_diag_ver[i].sort(new Comparator<BoardCell>() {
                    @Override
                    public int compare(BoardCell o1, BoardCell o2) {
                        return o1.getX() - o2.getX();
                    }
                });
            }

            algorithm_ver[i].sort(new Comparator<BoardCell>() {
                @Override
                public int compare(BoardCell o1, BoardCell o2) {
                    return o1.getX() - o2.getX();
                }
            });
            if (i <= tab.length / 2) {
                opponent_diag_ver[i].sort(new Comparator<BoardCell>() {
                    @Override
                    public int compare(BoardCell o1, BoardCell o2) {
                        return o1.getX() - o2.getX();
                    }
                });
            }

            opponent_ver[i].sort(new Comparator<BoardCell>() {
                @Override
                public int compare(BoardCell o1, BoardCell o2) {
                    return o1.getX() - o2.getX();
                }
            });
        }

        //looking for victory
        for (int i = 0; i < algorithm_hor.length; i++) {
            if (fourInARow(algorithm_hor[i]) != Integer.MAX_VALUE) {
                return new BoardCell(i, fourInAColumn(algorithm_hor[i]), null);
            } else if (fourInAColumn(algorithm_ver[i]) != Integer.MAX_VALUE) {
                return new BoardCell(fourInAColumn(algorithm_ver[i]), i, null);
            }/*else if(i<=tab.length/2 && fourInAColumn(algorithm_diag_ver[i])!= Integer.MAX_VALUE){
                return new BoardCell(fourInAColumn(algorithm_diag_ver[i]), i, null);
            }else if(i<=tab.length/2 && fourInARow(algorithm_diag_hor[i])!= Integer.MAX_VALUE){
                return new BoardCell(i, fourInAColumn(algorithm_diag_hor[i]), null);
            }*/
        }

        //looking for danger
        for (int i = 0; i < algorithm_hor.length; i++) {
            if (fourInARow(opponent_hor[i]) != Integer.MAX_VALUE) {
                return new BoardCell(i, fourInAColumn(opponent_hor[i]), null);
            } else if (fourInAColumn(opponent_ver[i]) != Integer.MAX_VALUE) {
                return new BoardCell(fourInAColumn(opponent_ver[i]), i, null);
            }/*else if(i<=tab.length/2 && fourInAColumn(opponent_diag_ver[i])!= Integer.MAX_VALUE){
                return new BoardCell(fourInAColumn(opponent_diag_ver[i]), i, null);
            }else if(i<=tab.length/2 && fourInARow(opponent_diag_hor[i])!= Integer.MAX_VALUE){
                return new BoardCell(i, fourInAColumn(opponent_diag_hor[i]), null);
            }*/
        }

        //other move
        int max = 0;
        LinkedList<BoardCell> where_to_move = null;
        String origin = "";

        for (LinkedList<BoardCell> list : algorithm_hor) {
            if (list.size() > max) {
                max = list.size();
                where_to_move = list;
                origin = "ah";
            }
        }
        for (LinkedList<BoardCell> list : algorithm_ver) {
            if (list.size() > max) {
                max = list.size();
                where_to_move = list;
                origin = "av";
            }
        }
        /*for(LinkedList<BoardCell> list: algorithm_diag_hor){
            if(list.size() > max) {
                max = list.size();
                where_to_move = list;
                origin = "adh";
            }
        }
        for(LinkedList<BoardCell> list: algorithm_diag_ver){
            if(list.size() > max) {
                max = list.size();
                where_to_move = list;
                origin = "adv";
            }
        }*/

        if (max == 0) {
            Random rand = new Random();
            return new BoardCell(rand.nextInt(10), rand.nextInt(10), null);
        } else {
            BoardCell resultCell = findNext(where_to_move, origin);
            if (resultCell == null || resultCell.getY() > 10 || resultCell.getX() > 10) {
                Random rand = new Random();
                int x = rand.nextInt(10);
                int y = rand.nextInt(10);
                while (tab[x][y] != 0) {
                    x = rand.nextInt(10);
                    y = rand.nextInt(10);
                }
                resultCell = new BoardCell(x, y, null);
            }
            return resultCell;
        }

    }

    private static BoardCell findNext(LinkedList<BoardCell> list, String origin) {
        if (list == null) return null;
        int max = 0;
        int counter = 0;
        int prev = -1;
        BoardCell result = null;
        for (BoardCell cell : list) {
            if (origin.equals("ah") || origin.equals("adh")) {
                if (cell.getY() == prev + 1) {
                    counter++;
                    if (counter > max) {
                        max = counter;
                        result = new BoardCell(cell.getX(), cell.getY() + 1, null);
                    }
                } else {
                    counter = 0;
                }
            } else {
                if (cell.getX() == prev + 1) {
                    counter++;
                    if (counter > max) {
                        max = counter;
                        result = new BoardCell(cell.getX() + 1, cell.getY(), null);
                    }
                } else {
                    counter = 0;
                }
            }
        }
        return result;
    }

    private static int fourInARow(LinkedList<BoardCell> list) {
        if (list.size() < 4) return Integer.MAX_VALUE;
        int counter = 4;
        int prevCol = Integer.MAX_VALUE;
        for (BoardCell cell : list) {
            if (cell.getY() == prevCol + 1) counter--;
            else {
                counter = 4;
            }
            prevCol = cell.getY();
            if (counter == 0) {
                if (prevCol >= 5) return prevCol - 4;
                else return prevCol + 4;
            }
        }
        return Integer.MAX_VALUE;
    }

    private static int fourInAColumn(LinkedList<BoardCell> list) {
        if (list.size() < 4) return Integer.MAX_VALUE;
        int counter = 4;
        int prevRow = Integer.MAX_VALUE;
        for (BoardCell cell : list) {
            if (cell.getX() == prevRow + 1) counter--;
            else {
                counter = 4;
                prevRow = cell.getX();
            }
            if (counter == 0) {
                if (prevRow >= 5) return prevRow - 4;
                else return prevRow + 4;
            }
        }
        return Integer.MAX_VALUE;
    }

    public static BoardCell MakeMove(int[][] tab) {
        /*
		// do przechowywania numerow naszej sytuacji
		ArrayList<ArrayList> wiersze = new ArrayList<ArrayList>();
		ArrayList<ArrayList> kolumny = new ArrayList<ArrayList>();
		// ruchy przeciwnika
		ArrayList<ArrayList> wierszeP = new ArrayList<ArrayList>();
		ArrayList<ArrayList> kolumnyP = new ArrayList<ArrayList>();
		*/
        int ciagPoziom = 0;
        int ciagPion = 0;
        //decyzja co do wiersza
        int decW = 9;
        //decyzja co do kolumny
        int decK = 9;
        //dlugosc ciagu jedynek lub -1
        int tymczasowyCiag = 0;

		/*// po tablicy stanu gry:
		for (int i = 0; i < 10; i++) {
			for (int j = 0; j < 10; j++) {

				// do arraylist wiersze w ideksach 0,1 itd. s¹ dodawane liczby kolumn, w ktorych
				// mamy swoje kolko
				// i analogicznie
				// do arraylist kolumny w ideksach 0,1 itd. s¹ dodawane liczby wierszy, w
				// ktorych mamy swoje kolko
				if (tab[i][j] == 1) {
					wiersze.get(i).add(j);
					kolumny.get(j).add(i);

				}

				// to samo z przeciwnikiem
				if (tab[i][j] == -1) {
					wierszeP.get(i).add(j);
					kolumnyP.get(j).add(i);

				}
*/
        // koniec dodawania do arraylist

        // analiza sytuacji

        /*
         * if ( (wiersze.get(x).contains(x)) && (wiersze.get(x).contains(x+1)) &&
         * (wiersze.get(x).contains(x+2)) && (wiersze.get(x).contains(x+3)) &&
         * (wiersze.get(x).contains(x+4))) { if()
         *
         *
         * }
         *
         * else if ((wiersze.get(x).contains(x)) && (wiersze.get(x).contains(x+1)) &&
         * (wiersze.contains(x+2)) && (wiersze.contains(x+3)) &&
         * (wiersze.contains(x+4))){
         *
         *
         *
         * } }
         */

        //przeciwnik
        for (int w = 0; w < 10; w++) {
            for (int k = 0; k < 10; k++) {
                if (tab[w][k] == -1) {
                    ciagPoziom++;
                }
                if (ciagPoziom > tymczasowyCiag && k != 9 && tab[w][k + 1] == 0) {
                    tymczasowyCiag = ciagPoziom;
                    decK = k + 1;
                    decW = w;
                } else if (ciagPoziom > tymczasowyCiag && k != 0 && tab[w][k - 1] == 0) {
                    tymczasowyCiag = ciagPoziom;
                    decK = k - 1;
                    decW = w;
                }
                if (tab[w][k] == 0 || tab[w][k] == 1) {
                    ciagPoziom = 0;
                }
                //pion
                if (tab[k][w] == -1) {
                    ciagPion++;
                }
                if (ciagPion > tymczasowyCiag && k != 9 && tab[k + 1][w] == 0) {
                    tymczasowyCiag = ciagPion;
                    decK = w;
                    decW = k + 1;
                }
                if (ciagPion > tymczasowyCiag && k != 0 && tab[k - 1][w] == 0) {
                    tymczasowyCiag = ciagPion;
                    decK = w;
                    decW = k - 1;
                }
                if (tab[k][w] == 1 || tab[k][w] == 0) {
                    ciagPion = 0;
                }
            }
        }
        if (tymczasowyCiag <= 3) {
            //wygrana
            for (int w = 0; w < 10; w++) {
                for (int k = 0; k < 10; k++) {
                    if (tab[w][k] == 1) {
                        ciagPoziom++;
                    }
                    if (ciagPoziom > tymczasowyCiag && k != 9 && tab[w][k + 1] == 0) {
                        tymczasowyCiag = ciagPoziom;
                        decK = k + 1;
                        decW = w;
                    } else if (ciagPoziom > tymczasowyCiag && k != 0 && tab[w][k - 1] == 0) {
                        tymczasowyCiag = ciagPoziom;
                        decK = k - 1;
                        decW = w;
                    }
                    if (tab[w][k] == -1 || tab[w][k] == 0) {
                        ciagPoziom = 0;
                    }
                    //w pionie
                    if (tab[k][w] == 1) {
                        ciagPion++;
                    }
                    if (ciagPion > tymczasowyCiag && w != 9 && tab[k + 1][w] == 0) {
                        tymczasowyCiag = ciagPion;
                        decK = k + 1;
                        decW = w;
                    } else if (ciagPion > tymczasowyCiag && w != 0 && tab[k - 1][w] == 0) {
                        tymczasowyCiag = ciagPion;
                        decK = w;
                        decW = k - 1;
                    }
                    if (tab[k][w] == -1 || tab[w][k] == 0) {
                        ciagPion = 0;
                    }
                }
            }
        }
        if (tab[decW][decK] == 0) {
            //tab[decW][decK]= -1;
            return new BoardCell(decW, decK, null);
        } else if (decW != 9 && tab[decW + 1][decK] == 0) {
            //tab[decW+1][decK]= -1;
            return new BoardCell(decW, decK, null);
        }
        //return tab;
        return null;
    }

    public static double[][] TestAlgo(double[][] tab) {
        /*
		// do przechowywania numerow naszej sytuacji
		ArrayList<ArrayList> wiersze = new ArrayList<ArrayList>();
		ArrayList<ArrayList> kolumny = new ArrayList<ArrayList>();
		// ruchy przeciwnika
		ArrayList<ArrayList> wierszeP = new ArrayList<ArrayList>();
		ArrayList<ArrayList> kolumnyP = new ArrayList<ArrayList>();
		*/
        int ciagPoziom = 0;
        int ciagPion = 0;
        //decyzja co do wiersza
        int decW = 9;
        //decyzja co do kolumny
        int decK = 9;
        //dlugosc ciagu jedynek lub -1
        int tymczasowyCiag = 0;

		/*// po tablicy stanu gry:
		for (int i = 0; i < 10; i++) {
			for (int j = 0; j < 10; j++) {

				// do arraylist wiersze w ideksach 0,1 itd. s¹ dodawane liczby kolumn, w ktorych
				// mamy swoje kolko
				// i analogicznie
				// do arraylist kolumny w ideksach 0,1 itd. s¹ dodawane liczby wierszy, w
				// ktorych mamy swoje kolko
				if (tab[i][j] == 1) {
					wiersze.get(i).add(j);
					kolumny.get(j).add(i);

				}

				// to samo z przeciwnikiem
				if (tab[i][j] == -1) {
					wierszeP.get(i).add(j);
					kolumnyP.get(j).add(i);

				}
*/
        // koniec dodawania do arraylist

        // analiza sytuacji

        /*
         * if ( (wiersze.get(x).contains(x)) && (wiersze.get(x).contains(x+1)) &&
         * (wiersze.get(x).contains(x+2)) && (wiersze.get(x).contains(x+3)) &&
         * (wiersze.get(x).contains(x+4))) { if()
         *
         *
         * }
         *
         * else if ((wiersze.get(x).contains(x)) && (wiersze.get(x).contains(x+1)) &&
         * (wiersze.contains(x+2)) && (wiersze.contains(x+3)) &&
         * (wiersze.contains(x+4))){
         *
         *
         *
         * } }
         */

        //przeciwnik
        for (int w = 0; w < 10; w++) {
            for (int k = 0; k < 10; k++) {
                if (tab[w][k] == -1) {
                    ciagPoziom++;
                }
                if (ciagPoziom > tymczasowyCiag && k != 9 && tab[w][k + 1] == 0) {
                    tymczasowyCiag = ciagPoziom;
                    decK = k + 1;
                    decW = w;
                } else if (ciagPoziom > tymczasowyCiag && k != 0 && tab[w][k - 1] == 0) {
                    tymczasowyCiag = ciagPoziom;
                    decK = k - 1;
                    decW = w;
                }
                if (tab[w][k] == 0 || tab[w][k] == 1) {
                    ciagPoziom = 0;
                }
                //pion
                if (tab[k][w] == -1) {
                    ciagPion++;
                }
                if (ciagPion > tymczasowyCiag && k != 9 && tab[k + 1][w] == 0) {
                    tymczasowyCiag = ciagPion;
                    decK = w;
                    decW = k + 1;
                }
                if (ciagPion > tymczasowyCiag && k != 0 && tab[k - 1][w] == 0) {
                    tymczasowyCiag = ciagPion;
                    decK = w;
                    decW = k - 1;
                }
                if (tab[k][w] == 1 || tab[k][w] == 0) {
                    ciagPion = 0;
                }
            }
        }
        if (tymczasowyCiag <= 3) {
            //wygrana
            for (int w = 0; w < 10; w++) {
                for (int k = 0; k < 10; k++) {
                    if (tab[w][k] == 1) {
                        ciagPoziom++;
                    }
                    if (k < 9 && ciagPoziom > tymczasowyCiag && k != 9 && tab[w][k + 1] == 0) {
                        tymczasowyCiag = ciagPoziom;
                        decK = k + 1;
                        decW = w;
                    } else if (k > 1 && ciagPoziom > tymczasowyCiag && k != 0 && tab[w][k - 1] == 0) {
                        tymczasowyCiag = ciagPoziom;
                        decK = k - 1;
                        decW = w;
                    }
                    if (tab[w][k] == -1 || tab[w][k] == 0) {
                        ciagPoziom = 0;
                    }
                    //w pionie
                    if (tab[k][w] == 1) {
                        ciagPion++;
                    }
                    if (k < 9 && ciagPion > tymczasowyCiag && w != 9 && tab[k + 1][w] == 0) {
                        tymczasowyCiag = ciagPion;
                        decK = k + 1;
                        decW = w;
                    } else if (k > 1 && ciagPion > tymczasowyCiag && w != 0 && tab[k - 1][w] == 0) {
                        tymczasowyCiag = ciagPion;
                        decK = w;
                        decW = k - 1;
                    }
                    if (tab[k][w] == -1 || tab[w][k] == 0) {
                        ciagPion = 0;
                    }
                }
            }
        }
        if (tab[decW][decK] == 0) {
            tab[decW][decK] = 1;
        } else if (decW != 9 && tab[decW + 1][decK] == 0) {
            tab[decW + 1][decK] = 1;
        }
        return tab;
    }

    public static BoardCell MakeAMove(int[][] tab) {

        int wiersz = 0;
        int kolumna = 0;
        int ciagPoziom = 0;
        int ciagPion = 0;
        int ciagTemp = 0;

        //przeciwnik

        //poziom
        for (int w = 0; w < 10; w++) {

            for (int k = 0; k < 10; k++) {

                if (tab[w][k] == -1) {

                    ciagPoziom++;

                }

                if (ciagPoziom > ciagTemp) {

                    if (k < 9 && tab[w][k + 1] == 0) {
                        wiersz = w;
                        kolumna = k + 1;
                        ciagTemp = ciagPoziom;

                    } else if (k > 0 && k < 9 && tab[w][k + 1] != 0 && tab[w][k - 1] == 0) {
                        wiersz = w;
                        kolumna = k - 1;
                        ciagTemp = ciagPoziom;

                    } else if (k > 0 && k < 9 && tab[w][k + 1] != 0 && tab[w][k - 1] != 0) {
                        ciagPoziom = 0;
                    }


                }
                if (tab[w][k] == 1 || tab[w][k] == 0) {

                    ciagPoziom = 0;

                }


            }
        }

        //przeciwnik pion

        for (int k = 0; k < 10; k++) {

            for (int w = 0; w < 10; w++) {

                if (tab[w][k] == -1) {

                    ciagPion++;

                }

                if (ciagPion > ciagTemp) {

                    if (tab[w + 1][k] == 0) {

                        wiersz = w + 1;
                        kolumna = k;
                        ciagTemp = ciagPion;

                    } else if (tab[w + 1][k] != 0 && w != 9 && tab[w + 1][k] == 0) {
                        wiersz = w + 1;
                        kolumna = k;
                        ciagTemp = ciagPion;

                    } else if (tab[w + 1][k] != 0 && k != 9 && tab[w + 1][k] != 0) {
                        ciagPion = 0;
                    }


                }

                if (tab[w][k] == 1 || tab[w][k] == 0) {

                    ciagPion = 0;

                }

            }
        }

        if (ciagTemp < 3) {

            ciagPoziom = 0;
            ciagPion = 0;
            ciagTemp = 0;

            //poziom
            for (int w = 0; w < 10; w++) {

                for (int k = 0; k < 10; k++) {

                    if (tab[w][k] == 1) {

                        ciagPoziom++;

                    }

                    if (ciagPoziom > ciagTemp) {

                        if (k < 9 && k != 9 && tab[w][k + 1] == 0) {
                            wiersz = w;
                            kolumna = k + 1;
                            ciagTemp = ciagPoziom;

                        } else if (k < 9 && tab[w][k + 1] != 0 && k > 0 && tab[w][k - 1] == 0) {
                            wiersz = w;
                            kolumna = k - 1;
                            ciagTemp = ciagPoziom;

                        } else if (k < 9 && tab[w][k + 1] != 0 && k > 0 && tab[w][k - 1] != 0) {
                            ciagPoziom = 0;
                        }


                    }
                    if (tab[w][k] == -1 || tab[w][k] == 0) {

                        ciagPoziom = 0;

                    }


                }
            }

            //pion

            for (int k = 0; k < 10; k++) {

                for (int w = 0; w < 10; w++) {

                    if (tab[w][k] == 1) {

                        ciagPion++;

                    }

                    if (ciagPion > ciagTemp) {

                        if (w < 9 && tab[w + 1][k] == 0) {
                            wiersz = w + 1;
                            kolumna = k;
                            ciagTemp = ciagPion;

                        } else if (w < 9 && tab[w + 1][k] != 0 && w != 9 && tab[w + 1][k] == 0) {
                            wiersz = w + 1;
                            kolumna = k;
                            ciagTemp = ciagPion;


                        } else if (w < 9 && tab[w + 1][k] != 0 && tab[w + 1][k] != 0) {
                            ciagPion = 0;
                        }


                    }

                    if (tab[w][k] == -1 || tab[w][k] == 0) {

                        ciagPion = 0;

                    }

                }
            }


        }
        Random rand = new Random();

        while ((tab[wiersz][kolumna] != 0) || (wiersz == 0 && kolumna == 0)) {

            wiersz = rand.nextInt(9);
            kolumna = rand.nextInt(9);


        }

        //tab[wiersz][kolumna]=1;

        return new BoardCell(wiersz, kolumna, null);

        //return tab;

    }

    public static double[][] MakeAMoveArray(int[][] tab) {

        int wiersz = 0;
        int kolumna = 0;
        int ciagPoziom = 0;
        int ciagPion = 0;
        int ciagTemp = 0;

        //przeciwnik

        //poziom
        for (int w = 0; w < 10; w++) {

            for (int k = 0; k < 10; k++) {

                if (tab[w][k] == -1) {

                    ciagPoziom++;

                }

                if (ciagPoziom > ciagTemp) {

                    if (k < 9 && tab[w][k + 1] == 0) {
                        wiersz = w;
                        kolumna = k + 1;
                        ciagTemp = ciagPoziom;

                    } else if (k > 0 && k < 9 && tab[w][k + 1] != 0 && tab[w][k - 1] == 0) {
                        wiersz = w;
                        kolumna = k - 1;
                        ciagTemp = ciagPoziom;

                    } else if (k > 0 && k < 9 && tab[w][k + 1] != 0 && tab[w][k - 1] != 0) {
                        ciagPoziom = 0;
                    }


                }
                if (tab[w][k] == 1 || tab[w][k] == 0) {

                    ciagPoziom = 0;

                }


            }
        }

        //przeciwnik pion

        for (int k = 0; k < 10; k++) {

            for (int w = 0; w < 10; w++) {

                if (tab[w][k] == -1) {

                    ciagPion++;

                }

                if (ciagPion > ciagTemp) {

                    if (tab[w + 1][k] == 0) {
                        wiersz = w + 1;
                        kolumna = k;
                        ciagTemp = ciagPion;

                    } else if (tab[w + 1][k] != 0 && w != 9 && tab[w + 1][k] == 0) {
                        wiersz = w + 1;
                        kolumna = k;
                        ciagTemp = ciagPion;

                    } else if (tab[w + 1][k] != 0 && k != 9 && tab[w + 1][k] != 0) {
                        ciagPion = 0;
                    }


                }

                if (tab[w][k] == 1 || tab[w][k] == 0) {

                    ciagPion = 0;

                }

            }
        }

        if (ciagTemp < 3) {

            ciagPoziom = 0;
            ciagPion = 0;
            ciagTemp = 0;

            //poziom
            for (int w = 0; w < 10; w++) {

                for (int k = 0; k < 10; k++) {

                    if (tab[w][k] == 1) {

                        ciagPoziom++;

                    }

                    if (ciagPoziom > ciagTemp) {

                        if (k < 9 && k != 9 && tab[w][k + 1] == 0) {
                            wiersz = w;
                            kolumna = k + 1;
                            ciagTemp = ciagPoziom;

                        } else if (k < 9 && tab[w][k + 1] != 0 && k != 0 && tab[w][k - 1] == 0) {
                            wiersz = w;
                            kolumna = k - 1;
                            ciagTemp = ciagPoziom;

                        } else if (k < 9 && tab[w][k + 1] != 0 && k != 0 && tab[w][k - 1] != 0) {
                            ciagPoziom = 0;
                        }


                    }
                    if (tab[w][k] == -1 || tab[w][k] == 0) {

                        ciagPoziom = 0;

                    }


                }
            }

            //pion

            for (int k = 0; k < 10; k++) {

                for (int w = 0; w < 10; w++) {

                    if (tab[w][k] == 1) {

                        ciagPion++;

                    }

                    if (ciagPion > ciagTemp) {

                        if (w < 9 && tab[w + 1][k] == 0) {
                            wiersz = w + 1;
                            kolumna = k;
                            ciagTemp = ciagPion;

                        } else if (w < 9 && tab[w + 1][k] != 0 && w != 9 && tab[w + 1][k] == 0) {
                            wiersz = w + 1;
                            kolumna = k;
                            ciagTemp = ciagPion;


                        } else if (w < 9 && tab[w + 1][k] != 0 && w != 9 && tab[w + 1][k] != 0) {
                            ciagPion = 0;
                        }


                    }

                    if (tab[w][k] == -1 || tab[w][k] == 0) {

                        ciagPion = 0;

                    }

                }
            }


        }
        Random rand = new Random();

        while ((tab[wiersz][kolumna] != 0) || (wiersz == 0 && kolumna == 0)) {

            wiersz = rand.nextInt(9);
            kolumna = rand.nextInt(9);


        }

        tab[wiersz][kolumna] = 1;

        double[][] array = new double[tab.length][tab[0].length];
        for (int i = 0; i < tab.length; i++) {
            for (int j = 0; j < tab[0].length; j++) {
                array[i][j] = tab[i][j];
            }
        }
        return array;

    }

}
