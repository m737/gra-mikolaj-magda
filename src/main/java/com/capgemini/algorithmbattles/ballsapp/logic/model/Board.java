package com.capgemini.algorithmbattles.ballsapp.logic.model;

import com.capgemini.algorithmbattles.ballsapp.logic.BoardDrawer;

public class Board {

  private static final int SIZE = 10;
  private Player[][] board = new Player[SIZE][SIZE];

  public void placeMove(BoardCell move) {
    board[move.getX()][move.getY()] = move.getPlayer();
  }

  public BoardCell getFirstEmptyCell() {
    for (int i = 0; i < SIZE; i++) {
      for (int j = 0; j < SIZE; j++) {
        if (board[i][j] == null) {
          return new BoardCell(i, j, null);
        }
      }
    }
    return null;
  }

  public int[][] getBoard(){
    int[][] board_temp = new int[SIZE][SIZE];
    for(int i = 0; i < SIZE; i++){
      for(int j = 0; j < SIZE; j++){
        if(board[i][j] != null) {
            if (board[i][j].toString().equals("player1")) board_temp[i][j] = 1;
            else if (board[i][j].toString().equals("player2")) board_temp[i][j] = -1;
          }
      }
    }
    return board_temp;
  }
  public Player[][] getBoardB(){
    return board;
  }

  public BoardCell getBoardCell(int x, int y){
    if(x < SIZE && y < SIZE) return new BoardCell(x, y, board[x][y]);
    else return null;
  }

  public double[] getInput(){
    double[] input = new double[SIZE*SIZE];
    int iter = 0;
    for(int i = 0; i < SIZE; i++){
      for(int j = 0; j < SIZE; j++){
        input[iter] = getBoard()[j][i];
        iter++;
      }
    }
    return input;
  }


  public boolean isGameFinished() {
    // TODO: Please implement it.
    return false;
  }

  public void printBoard() {
    BoardDrawer.drawBoard(this.board);
  }

}
