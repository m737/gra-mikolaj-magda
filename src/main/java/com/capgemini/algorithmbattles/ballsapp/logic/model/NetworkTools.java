package com.capgemini.algorithmbattles.ballsapp.logic.model;

public class NetworkTools {

    public static double[] createArray(int size, double value){
        double [] array = new double[size];
        for(int i = 0; i< array.length; i++) array[i] = value;
        return array;
    }

    public static double randomValue(double lower_bound, double upper_bound){
        return Math.random()*(upper_bound-lower_bound) + lower_bound;
    }

    public static double[] createRandomArray(int size, double lower_bound, double upper_bound){
        if(size < 1) return null;
        double[] array = new double[size];
        for(int i = 0; i<array.length; i++){
            array[i] = randomValue(lower_bound, upper_bound);
        }
        return array;
    }

    public static double[][] createRandomArray(int sizeX, int sizeY, double lower_bound, double upper_bound){
        if(sizeX < 1 || sizeY < 1) return null;
        double [][] array = new double[sizeX][sizeY];
        for(int i = 0; i < sizeX; i++){
            array[i] = createRandomArray(sizeY, lower_bound, upper_bound);
        }
        return array;
    }

    public static Integer[] randomValues (int amount,  int lower_bound, int upper_bound){

        lower_bound--;

        if(amount > (upper_bound - lower_bound)){
            return null;
        }

        Integer[] values = new Integer[amount];
        for(int i = 0; i < values.length; i++){
            int n = (int) randomValue(lower_bound, upper_bound);
            while(containsValue(values, n)){
                n = (int) randomValue(lower_bound, upper_bound);
            }
            values[i] = n;
        }
        return values;
    }

    private static <T extends Comparable<T>> boolean containsValue(T[] array, T value){
        for(int i = 0; i<array.length; i++){
            if(array[i] != null){
                if(array[i] == value){
                    return true;
                }
            }
        }
        return false;
    }

    public static int indexOfHighestValue(double [] values){
        int index = 0;
        for(int i = 0; i < values.length; i++){
            if(values[i] > values[index]){
                index = i;
            }
        }
        return index;
    }
    public static int indexOfLowestValue(double [] values){
        int index = 0;
        for(int i = 0; i < values.length; i++){
            if(values[i] < values[index]){
                index = i;
            }
        }
        return index;
    }
    public static double[] convertToInput(double[][] tab){
        double[] input = new double[tab.length*tab[0].length];
        int index = 0;
        for(int i = 0; i < tab.length; i++){
            for(int j = 0; j < tab[0].length; j++){
                input[index] = tab[i][j];
                index++;
            }
        }
        return input;
    }



}
