package com.capgemini.algorithmbattles.ballsapp.solution;

import com.capgemini.algorithmbattles.ballsapp.logic.BoardDrawer;
import com.capgemini.algorithmbattles.ballsapp.logic.model.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Random;

import static java.lang.Math.pow;

public class GamePlayer {

  private Player player;
  private Board board = new Board();
  private Network network;
  private int last_result;

  public GamePlayer(Player player) {
    Random rand = new Random();
    this.player = player;
    this.last_result = -1;
    this.network = new Network(100, 200, 100);
    network.load("network.net");

  }

  /**
   * The application should calculate the next move after this method call.
   *
   * @return the next {@link BoardCell move} for current player.
   */
  public BoardCell nextMove() {
    BoardCell cell = getCellForNextMove();
    cell.setPlayer(player);
    board.placeMove(cell);
    return cell;
  }

  private BoardCell getCellForNextMove() {
    // TODO: Please implement it.
    //for(int i = 0; i < board.getBoard().length; i++)System.out.println(Arrays.toString(board.getBoard()[i]));
    //System.out.println(Arrays.toString(board.getInput()));
    //BoardCell firstEmptyCell = board.getFirstEmptyCell();
    //firstEmptyCell.setPlayer(player);

    /*double[] output = network.calculate(board.getInput());
    int result = NetworkTools.indexOfHighestValue(output);
    System.out.println(result); //Network's decision
    last_result = result;
    return getResult(result);*/

    //return Training.MakeMove(board.getBoard());

    //System.out.println(Training.Algorithm2Cell(board.getBoard()).getX());
    //System.out.println(Training.Algorithm2Cell(board.getBoard()).getY());
    network.learn(board.getInput(), NetworkTools.convertToInput(Training.MakeAMoveArray(board.getBoard())), 1);
    network.save("network.net");
    return Training.MakeAMove(board.getBoard());
  }

  private BoardCell getResult(int index){
    for(int i = 0; i < board.getBoard().length; i++){
      for(int j = 0; j < board.getBoard().length; j++){
        if(index == 1) return board.getBoardCell(j, i);
        else index--;
      }
    }
    return null;
  }

  /**
   * The opponent made the move passed in param.
   *
   * @param move the {@link BoardCell} made by the opponent.
   */
  public void moveMadeByOpponent(BoardCell move) {
    this.board.placeMove(move);
  }

  /**
   * @return true if the game is finished
   */
  public boolean isGameFinished() {
    return this.board.isGameFinished();
  }

  /**
   * Draw the board on the console.
   */
  public void printBoard() {
    this.board.printBoard();
  }
}
